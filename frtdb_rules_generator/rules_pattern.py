"""
A module containing the template class to represent a node in Firebase Realtime Database
"""
import sys

class RulesPattern:
	"""
	A class to represent a node in Firebase Realtime Database rules
	"""

	def __init__(self, label="", variables=None):
		self.set_variables(variables)
		self.set_label(label)
		self.set_read("")
		self.set_write("")
		self.set_validate("")
		self.index_on = []
		self.set_inline(False)

		self.rules_list = []
		self.__build__()

	def __build__(self):
		"""
		Method to implement by nodes in order to build themselves and add subnodes
		"""
		raise NotImplementedError

	def add(self, rule):
		"""
		Adds a child node
		"""
		if self.label == "":
			print("Error: Node label should be defined before adding rules")
			sys.exit(1)
		self.rules_list.append(rule)
		return rule

	def set_label(self, label):
		"""
		A setter for field "label"
		"""
		self.label = label
		if self.label.startswith("$"):
			if self.label in self.variables:
				print("Error: Variable '" + self.label + "' has already been defined")
				sys.exit(1)
			self.variables.append(self.label)

	def set_read(self, read):
		"""
		A setter for field "read"
		"""
		self.read = read

	def set_write(self, write):
		"""
		A setter for field "write"
		"""
		self.write = write

	def set_validate(self, validate):
		"""
		A setter for field "validate"
		"""
		self.validate = validate

	def set_inline(self, inline=True):
		"""
		A setter for field "inline"
		"""
		self.inline = inline

	def set_variables(self, variables):
		"""
		A setter for field "variables"
		"""
		self.variables = []
		if variables:
			for variable in variables:
				self.variables.append(variable)

	def check_variable(self, variable):
		"""
		Check if the given variable has been defined in parent nodes
		"""
		if variable in self.variables:
			return variable
		print("Error: variable '" + variable + "' is not defined in node '" + self.label + "'")
		sys.exit(1)

	def inline_node_to_json(self, path):
		"""
		Returns the JSON content of an inline node.
		Inline node should only have a ".validate" rule.
		path: the path to the current node
		"""
		line = ""
		line_count = 0

		if self.read != "":
			line_count += 1
			line = '"' + self.label + '": { ".read" : "' + self.read + '" },\n'
		if self.write != "":
			line_count += 1
			line = '"' + self.label + '": { ".write" : "' + self.write + '" },\n'

		if self.validate != "":
			line_count += 1
			line = '"' + self.label + '": { ".validate" : "' + self.validate + '" },\n'
		else:
			print("Warning: Inline node '" + path + "' does not have '.validate' rule")

		if self.index_on != []:
			print("Warning: Inline node '" + path + "': '.indexOn' ignored")

		if line_count > 1:
			print("Error: Inline node '" + path + "' cannot have multiple rules")
			sys.exit(1)

		return line

	def to_json_block(self, path=""):
		"""
		Convert the tree structure into JSON like lines to being compiled
		path: the path to the parent node
		"""
		path = path + "/" + self.label
		lines = []

		if self.inline:
			lines.append(self.inline_node_to_json(path))

		else:
			lines.append('"' + self.label + '": {\n')

			if self.read != "":
				lines.append('\t".read" : "' + self.read + '",\n')
			if self.write != "":
				lines.append('\t".write" : "' + self.write + '",\n')
			if self.validate != "":
				lines.append('\t".validate" : "' + self.validate + '",\n')
			if self.index_on != []:
				lines.append('\t".indexOn" : ' + str(self.index_on).replace("'", '"') + ',\n')

			list_exists = False

			for rule in self.rules_list:
				list_exists = list_exists or rule.label.startswith("$")
				for line in rule.to_json_block(path=path):
					lines.append("\t" + line)

			if not (list_exists or self.validate):
				print("Warning: No list or '.validate' rule in node '" + path + "'")

			lines.append('},\n')

		return lines

	def to_json(self):
		"""
		Entry point for converting the tree into JSON content
		"""
		string = "{\n"
		for line in self.to_json_block():
			string += "\t" + line
		string += "}\n"
		return string


	def get_puml_block_properties(self):
		"""
		Return a PlantUML summary of read, write, validate and indexOn rules
		"""
		properties = []
		if self.read != "":
			properties.append("r")
		if self.write != "":
			properties.append("w")
		if self.validate != "":
			properties.append("v")
		if self.index_on != []:
			properties.append("i")
		return ", ".join(properties)

	def to_puml_block(self, count=None, leaves=False, depth=255):
		"""
		Convert the tree structure into PlantUML block
		leaves: a round where only looking for leaves
		count: a count value wrapped in a list to attribute unique ID to nodes
		depth: the depth of the subtree to draw
		"""

		if leaves:
			if self.inline:
				return "\t" + self.label + ": " + self.get_puml_block_properties() + "\n"
			return ""

		m_count = count[0]
		count[0] += 1

		parent_name = self.label + "_" + str(m_count)
		content = "\nobject " + parent_name + " {\n"
		content += "[" + self.get_puml_block_properties() + "]\n"
		for rule in self.rules_list:
			content += rule.to_puml_block(leaves=True)
		content += "}\n"

		if depth <= 0:
			return content

		for rule in self.rules_list:
			if not rule.inline:
				i_count = count[0]
				content += rule.to_puml_block(count=count, depth=depth-1)
				child_name = rule.label + "_" + str(i_count)
				child_tag = "*" if rule.label.startswith("$") else "1"
				content += parent_name + " *% \"" + child_tag + "\" " + child_name + "\n"

		return content

	def to_plantuml(self, depth=255):
		"""
		Entry point for converting the tree into PlantUML diagram
		"""
		content = "@startuml\n"
		count = [0]
		content += self.to_puml_block(count=count, depth=depth)
		content += "\n@enduml\n"
		return content.replace("-", "_").replace("%", "--").replace("$", "X_")
