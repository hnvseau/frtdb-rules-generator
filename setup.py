"""
Setup script for PIP module
"""
import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
		name="frtdb_rules_generator",
		version="1.0.0",
		description="A tool to gererate Firebase Real Time Database rules",
		long_description=README,
		long_description_content_type="text/markdown",
		url="https://gitlab.com/hnvseau/frtdb-rules-generator",
		# author="",
		# author_email="",
		# license="MIT",
		classifiers=[
				# "License :: OSI Approved :: MIT License",
				"Programming Language :: Python :: 3",
				"Programming Language :: Python :: 3.7",
		],
		packages=["frtdb_rules_generator"],
		include_package_data=True,
		install_requires=[]
)
