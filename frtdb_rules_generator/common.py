"""
A module defining genric rules (for data types) and logic functions
"""
from .rules_pattern import RulesPattern

class OtherRules(RulesPattern):
	"""
	The rule to prevent adding additional data to a node
	"""
	def __build__(self):
		self.set_label("$other")
		self.set_validate("false")
		self.set_inline(True)

class StringRules(RulesPattern):
	"""
	A rules to valide a string
	"""
	def __init__(self, label, length):
		self.length = length
		super().__init__(label)

	def __build__(self):
		self.set_validate(self.validate_string(self.length))
		self.set_inline(True)

	@staticmethod
	def validate_string(length):
		"""
		Validates that new data is of type string and has limited size
		"""
		return "newData.isString() && newData.val().length <= " + str(length)

class NumberRules(RulesPattern):
	"""
	A rule to validate a number
	"""
	def __build__(self):
		self.set_validate(self.validate_number())
		self.set_inline(True)

	@staticmethod
	def validate_number():
		"""
		Validates that new data is of type number
		"""
		return "newData.isNumber()"

class BornedNumberRules(RulesPattern):
	"""
	A rule to validate a borned number
	"""
	def __init__(self, label, min_v, max_v):
		self.min_v = min_v
		self.max_v = max_v
		super().__init__(label)

	def __build__(self):
		self.set_validate(self.validate_borned_number(self.min_v, self.max_v))
		self.set_inline(True)

	@staticmethod
	def validate_borned_number(min_v, max_v):
		"""
		Validates that new data is of type number and is borned
		min_v: minumum value for the number
		max_v: maximum value for the number
		"""
		validate = NumberRules.validate_number()
		validate += " && newData.val() >= " + str(min_v)
		validate += " && newData.val() <= " + str(max_v)
		return validate

class BooleanRules(RulesPattern):
	"""
	A rule to validate a boolean
	"""
	def __build__(self):
		self.set_validate(self.validate_boolean())
		self.set_inline(True)

	@staticmethod
	def validate_boolean():
		"""
		Validates that new data is of type boolean
		"""
		return "newData.isBoolean()"

class IdRules(RulesPattern):
	"""
	A rule to validate that new data is the on expected
	"""
	def __init__(self, label, id_val):
		self.id_val = id_val
		super().__init__(label)

	def __build__(self):
		self.set_validate(self.validate_identity(self.id_val))
		self.set_inline(True)

	@staticmethod
	def validate_identity(expected):
		"""
		Validates that new data isidentical to the expected one
		"""
		return "newData.val() === " + expected


def do_and(condition_list):
	"""
	A function that computes and AND between the conditions listed
	"""
	return "(" + ") && (".join(condition_list) + ")"

def do_or(condition_list):
	"""
	A function that computes and OR between the conditions listed
	"""
	return "(" + ") || (".join(condition_list) + ")"

def do_not(condition):
	"""
	A function that computes NOT of the given condition
	"""
	return "!(" + condition + ")"
