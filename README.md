# frtdb-rules-generator

A tool to generate Firebase Real Time Database rules


## Local installation

To install locally the PIP module, run the following command from this folder:
```
python3 -m pip install -e .
```
